package me.nipnacks.simplifiedcommands;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public final class SimplifiedCommands extends JavaPlugin implements CommandExecutor {

    @Override
    public void onEnable() {
        // Plugin startup logic
        getLogger().info(ChatColor.LIGHT_PURPLE + "SimplifiedCommands is now starting up!");
        System.out.println(ChatColor.LIGHT_PURPLE+"SimplifiedCommands is now Running!");
    }
    private ArrayList<Player> flying = new ArrayList<Player>();
    private ArrayList<Player> god = new ArrayList<Player>();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(sender instanceof Player) {

            Player p = (Player) sender;
            Location loc = p.getLocation();

                if (command.getName().equalsIgnoreCase("fly")) {
                    if (p.hasPermission("fly.perm")) {
                    if (!flying.contains(p)) {
                        flying.add(p);
                        p.sendMessage(ChatColor.AQUA + "Fly mode has been activated!");
                        p.setAllowFlight(true);
                    } else if (flying.contains(p)) {
                        flying.remove(p);
                        p.sendMessage(ChatColor.AQUA + "Fly mode has been deactivated!");
                        p.setAllowFlight(false);
                    }
                } else {
                        sender.sendMessage(ChatColor.RED + "You need to have the permission" + ChatColor.BLUE + "[" + ChatColor.DARK_RED + "fly.perm" + ChatColor.BLUE + "]");
                    }
            }

                if(command.getName().equalsIgnoreCase("god")) {
                    if (p.hasPermission("god.perm")){
                    if (!god.contains(p)) {
                        god.add(p);
                        p.sendMessage(ChatColor.AQUA + "God mode Enabled!");
                        p.setInvulnerable(true);
                    } else if (god.contains(p)) {
                        god.remove(p);
                        p.sendMessage(ChatColor.AQUA + "God mode Disabled!");
                        p.setInvulnerable(false);
                    }
                }else {
                        sender.sendMessage(ChatColor.RED + "You need to have the permission" + ChatColor.BLUE + "[" + ChatColor.DARK_RED + "god.perm" + ChatColor.BLUE + "]");
                    }
            }

                if (command.getName().equalsIgnoreCase("gmc")){
                    if (p.hasPermission("gmc.perm")) {
                    p.sendMessage(ChatColor.AQUA + "Gamemode Creative!");
                    p.setGameMode(GameMode.CREATIVE);
                }else {
                        sender.sendMessage(ChatColor.RED + "You need to have the permission" + ChatColor.BLUE + "[" + ChatColor.DARK_RED + "gmc.perm" + ChatColor.BLUE + "]");
                    }
            }

                if (command.getName().equalsIgnoreCase("gms")){
                    if (p.hasPermission("gms.perm")) {
                    p.sendMessage(ChatColor.AQUA + "Gamemode Survival!");
                    p.setGameMode(GameMode.SURVIVAL);
                }else {
                        sender.sendMessage(ChatColor.RED + "You need to have the permission" + ChatColor.BLUE + "[" + ChatColor.DARK_RED + "gms.perm" + ChatColor.BLUE + "]");
                    }
            }

                if (command.getName().equalsIgnoreCase("gma")){
                    if (p.hasPermission("gma.perm")) {
                    p.sendMessage(ChatColor.AQUA + "Gamemode Adventure!");
                    p.setGameMode(GameMode.ADVENTURE);
                }else {
                        sender.sendMessage(ChatColor.RED + "You need to have the permission" + ChatColor.BLUE + "[" + ChatColor.DARK_RED + "gma.perm" + ChatColor.BLUE + "]");
                    }
            }

                if (command.getName().equalsIgnoreCase("gmsp")){
                    if (p.hasPermission("gmsp.perm")) {
                    p.sendMessage(ChatColor.AQUA + "Gamemode Spectator!");
                    p.setGameMode(GameMode.CREATIVE);
                }else {
                        sender.sendMessage(ChatColor.RED + "You need to have the permission" + ChatColor.BLUE + "[" + ChatColor.DARK_RED + "gmsp.perm" + ChatColor.BLUE + "]");
                    }
            }


                if (command.getName().equalsIgnoreCase("exp")){
                    if (p.hasPermission("exp.perm")){
                    p.sendMessage(ChatColor.AQUA + "You have been given EXP!");
                    p.giveExp(10);
                }else {
                        sender.sendMessage(ChatColor.RED + "You need to have the permission" + ChatColor.BLUE + "[" + ChatColor.DARK_RED + "exp.perm" + ChatColor.BLUE + "]");

                    }
            }

                if (command.getName().equalsIgnoreCase("heal")){
                    if (p.hasPermission("hp.perm")){
                    p.setHealth(20);
                    p.sendMessage(ChatColor.AQUA + "You have now been Healed!");
                }else {
                        sender.sendMessage(ChatColor.RED + "You need to have the permission" + ChatColor.BLUE + "[" + ChatColor.DARK_RED + "hp.perm" + ChatColor.BLUE + "]");
                    }
            }

                if (command.getName().equalsIgnoreCase("feed")){
                    if (p.hasPermission("feed.perm")){
                    p.setFoodLevel(20);
                    p.sendMessage(ChatColor.AQUA + "You have now been Fed!");
                }else {
                        sender.sendMessage(ChatColor.RED + "You need to have the permission" + ChatColor.BLUE + "[" + ChatColor.DARK_RED + "feed.perm" + ChatColor.BLUE + "]");
                    }
            }

                if (command.getName().equalsIgnoreCase("suicide")){
                    if (p.hasPermission("suicide.perm")){
                    loc.getWorld().createExplosion(loc, 5);
                    p.sendMessage(ChatColor.AQUA + "Kaboom! You have Exploded!");
                    p.setHealth(-0);
                }else {
                        sender.sendMessage(ChatColor.RED + "You need to have the permission" + ChatColor.BLUE + "[" + ChatColor.DARK_RED + "suicide.perm" + ChatColor.BLUE + "]");
                    }
            }
        }else {
            sender.sendMessage(ChatColor.RED + "You need to be a player to do this Command!");
        }

        return false;
    }



    @Override
    public void onDisable() {
        // Plugin shutdown logic
        getLogger().info(ChatColor.LIGHT_PURPLE + "SimplifiedCommands is now Shutting down");
    }
}
